// bai 1
/**
 * dau vao : luong hang ngay, so ngay lam viec
 * cac buoc xu ly:
 * 1. Tao hang so luong hang ngay salary va gan gia tri 100000 
 * 2. Tao bien so ngay lam viec day va gan gia tri 22
 * 3. tinh tong luong trong thang bang totalSalary = salary *day
 * 4. in ket qua ra console
 * 
 * dau ra: tong luong trong thang
 */


const salary = 100000;
var day = 22;
var totalSalary = salary * day;
console.log ("bai 1: tong luong trong thang",{totalSalary});

// bai 2
/**
 * dau vao : 5 so thuc
 * cac buoc xu ly:
 * 1. Tao bien cho 5 so thuc va gan gia tri
 * num1 =2, num2=3, num3=5.5, num4=10/3, num5 = -9
 * 2. tinh trung binh cong cua 5 so thuc avg= (num1 + num2 +num3+ num4+num5)/5;
 * 3. in ket qua ra console
 * 
 * dau ra: gia tri trung binh cua 5 so thuc
 */

var num1 = 2;
var num2 = 3;
var num3 = 5.5;
var num4 = 10/3;
var num5 = -9;
var avg= (num1 + num2 +num3+ num4+num5)/5;
console.log("bai 2: So trung binh",{avg});

// bai 3
/**
 * dau vao : don gia USD, so tien USD can quy doi thanh VND
 * cac buoc xu ly:
 * 1. Tao hang so va gan gia tri cho don gia dong USD donGiaUSD =23500
 * 2. Tao bien va gan gia tri cho so tien USD can quy doi USDamt = 80
 * 3. Tao bien va gan gia tri cho so tien VND quy doi bang cong thuc VNDamt = USDamt * donGiaUSD
 * 4. in ket qua ra console
 * 
 * dau ra: so tien VND quy doi
 */


const donGiaUSD = 23500;
var USDamt = 80;
var VNDamt = USDamt * donGiaUSD;
console.log("bai 3: Tien VND",{VNDamt});

// bai 4
/**
 * dau vao : chieu dai va chieu rong
 * cac buoc xu ly:
 * 1. tao bien va gan gia tri cho chieu dai =20, rong=70
 * 2. tao bien va gan gia tri cho Dien tich theo cong thuc S = dai*rong
 * 3. tao bien va gan gia tri cho chu vi theo cong thuc  D = (dai+rong)*2;
 * 4. in ket qua chu vi va dien tich ra console 
 * 
 * dau ra: chu vi va dien tich
 */

var dai =30;
var rong = 70;

var S = dai*rong;
var D = (dai+rong)*2;

console.log("bai 4: Chu vi",{D},",Dien tich:",{S});


// bai 5
/**
 * dau vao : so co 2 chu so
 * cac buoc xu ly:
 * 1. tao bien va gan gia tri cho so co 2 chu so
 * 2. tao bien va gan gia tri cho so hang chu theo cong thuc soHangChuc = Math.floor(Digit/10)
 * 3. tao bien va gan gia tri cho so hang don vi theo cong thuc  soHangDonVi= Digit%10
 * 4. tao bien va gan gia tri tong 2 ky so theo cong thuc sum = soHangChuc +soHangDonVi
 * 5. in ket qua ra console 
 * 
 * dau ra: tong 2 ky so
 */

var Digit = 78;

var soHangChuc = Math.floor(Digit/10);
var soHangDonVi= Digit%10;

var sum = soHangChuc +soHangDonVi;
console.log("bai 5: Tong 2 ky so:",{sum})